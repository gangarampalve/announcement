<?php
/**
 * @varsion 	2.0.1
 * @author 		Rakesh Salunke <rakesh.salunke@pinstorm.com>
 * @copyright 	Pinstorm Technologies Pvt. Ltd.
 * @link 		www.pinstorm.com
 * @created_at	22 Feb 2018
 */


class Harvest 
{
	
	/**
	 * initilize Class
	 * @param Object $mysqli is object of class.mysqli.php which is used to interract with database.
	 */
	function __construct($mysqli){
		$this->mysqli = $mysqli;		
	}


	/**
     * debugger : echo string with time on time of execution
     * @param  [string] $msg [string to print]
     * @return [null]      
     */
    public function echostr($msg){
       echo "\r\n<br>\r\n";
       echo "[".date('Y-m-d h:i:s')."] ";
       echo $msg;
       echo "\r\n<br>\r\n";
    }

	/**
     * debugger : Print set of array
     * @param  [array] $data  printable data
     * @param  [string] $title [array name]
     * @param  [string] $exit  [set 1 to stop execution after printing string.]
     * @return [null]      
     */
	public function printdata($data,$title=null,$exit=null){
		echo '<pre>';
		echo $title.'==>';
		print_r($data);
		echo '</pre>';
		if(!empty($exit))
			exit;
	}

	/**
     * print json data
     * @param  [json] $data  [json string to print]
     * @param  [string] $title [assign title to json string]
     * @param  [string] $exit  [set 1 to stop execution after printing string.]
     * @return [null]   
     */
	public function jsonprint($data,$title=null,$exit=null){	
		echo '<pre>';
		echo $title.'==>';
		json_encode($data, JSON_PRETTY_PRINT);
		echo '</pre>';
		if(!empty($exit))
			exit;
	}
	/**
	 * Return clean string 
	 * @param  string $str [A String which is to be cleaned]
	 * @return [string]      [A clean string]
	 */
	public function cleanString($str){
		return stripcslashes(strip_tags(htmlspecialchars_decode($str)));

	}


	/**
	 * Clean String 
	 */
	
	public function clean($str){       
	    // $str = utf8_decode($str);
	    $str = html_entity_decode($str,ENT_QUOTES);
	    $str = htmlspecialchars_decode($str);
	    $str = str_replace("&nbsp;", "", $str);
	    $str = preg_replace("/\s+/", " ", $str);
	    $str = strip_tags($str);
	    $str = trim($str);
	    return $str;
	}

	/**
	 * Return array converted to object
	 * @param  [Object] $d 	Object to array
	 * @return [array]    	Return array converted to object
	 */
	public function objectToArray($d){
		if (is_object($d)) {
			$d = get_object_vars($d);
		}

		if (is_array($d)) {
			return array_map(__FUNCTION__, $d);
		}
		else {			
			return $d;
		}
	}



	/**
	 * Return array converted to object
	 * @param  [array] $d Convert array $d to object
	 * @return [object] 
	 */
	public function arrayToObject($d){
		if (is_array($d)) {
			/*
			* Using __FUNCTION__ (Magic constant)
			* for recursive call
			*/
			return (object) array_map(__FUNCTION__, $d);
		}
		else {
			// Return object
			return $d;
		}
	}


	public function curl_shell($curl_url){
		$curl_response = shell_exec("curl -L ".$curl_url);
		return $curl_response;
	}


	/**
	 * Return Curl Response
	 * @param  [String]  $url   Curl URL
	 * @param  [Post Data]  $postdata    will send additional data to given url
	 * @param  integer $dataRequire If $dataRequire is 1 then response will return else return true
	 * @return [string]	Based on $dataRequire Returns true or curl response
	 */
	public function curlRequest($curlUrl,$postdata=null,$dataRequire=1, $useragent=1){		
		$this->echostr($curlUrl);
		$curl = curl_init();		
		curl_setopt($curl, CURLOPT_URL,$curlUrl);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);	

		curl_setopt( $curl, CURLOPT_AUTOREFERER, true );


		if(isset($useragent) && !empty($useragent)):
			$readUserAgentFile = file('/var/www/html/work/project/xp1.orm.pinstorm.com/code/user-agents.txt');	

			//echo "RandomLine From user-agents.txt ===>";
			$userAgent =  $readUserAgentFile[array_rand($readUserAgentFile)];
			// $this->echostr($userAgent);
			curl_setopt($curl, CURLOPT_USERAGENT, $userAgent );	

		endif;
			
		curl_setopt($curl, CURLOPT_TIMEOUT, 60);			
		curl_setopt($curl, CURLOPT_ENCODING, 'gzip,deflate,sdch');	

		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_COOKIEFILE, "cookie.txt");
		curl_setopt($curl, CURLOPT_COOKIEJAR, "cookie.txt");

		if(!empty($postdata)){
			curl_setopt($curl, CURLOPT_POST, true);			
			curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);			
		}else{
			curl_setopt($curl, CURLOPT_HTTPGET, true);			
		}
		
		$response = curl_exec($curl);
		
		

		if(!curl_errno($curl)){
			$info = curl_getinfo($curl);
			// $this->printdata($info);				
			if($info['http_code'] == '200'){
				if($dataRequire==1)
					return $response;
				else
					return true;				
			}elseif($info['http_code'] == '400'){
				$response = $this->curl_shell($curlUrl);
				return $response;
			}
		}else{
			return false;
		}
		curl_close($curl);
	}



	/**
	 * Send Mail 
	 * @param  [string] $to      Receiver Email id
	 * @param  [type] $subject   Subject of mail
	 * @param  [type] $message   mailer body
	 * @return [type]          true or false
	 */
	public function sendmail($to, $subject, $message){

		if(isset($to) && !empty($to)){
			$headers = "From: ".PHPMAILER_FROM_NAME."<".PHPMAILER_FROM.">\r\n";
			$headers .= "Reply-To: ".PHPMAILER_FROM_NAME."<".PHPMAILER_FROM.">\r\n";
			$headers .= "CC: $to\r\n";
			$headers .= "MIME-Version: 1.0\r\n";
			$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
			mail($to, $subject, $message, $headers);
		}

	}




	/**
	 * Send mail using PHPMailer class
	 * @param  array $emailid    	List of email ids
	 * @param  String $subject  	Mail subject
	 * @param  [String] $msg     	Mailer body
	 * @param  [String] $attachment File path 
	 * @return [String]             Success or Error message
	 */
	public function phpmailer($emailid,$subject,$msg,$attachment=null){

		require_once('class.phpmailer.php');

        $mail = new PHPMailer(); // create a new object
        $mail->IsSMTP(); // enable SMTP
        $mail->Mailer = PHPMAILER_PROTOCOL; 
        $mail->SMTPDebug = 0; // debugging: 0 =  off errors, 1 = errors and messages, 2 = messages only       
        $mail->SMTPAuth = true; // authentication enabled
        $mail->SMTPSecure = PHPMAILER_SSL; // secure transfer enabled REQUIRED for GMail
        $mail->Host = PHPMAILER_HOST;
        $mail->Port = PHPMAILER_PORT; // or 587
        $mail->IsHTML(true);
        $mail->Username = PHPMAILER_EMAILID;
        $mail->Password = PHPMAILER_EMAILID_PASS;
        $mail->SetFrom(PHPMAILER_FROM, PHPMAILER_FROM_NAME, true);
        // $mail->SetFrom("apps@pinstorm.com", "Zattu", true);
        // $mail->AddReplyTo('rakesh@pinstorm.com', 'First Last');
        $mail->Subject = $subject;
        //$mail->Body = $mailContent;

        //Read an HTML message body from an external file, convert referenced images to embedded, convert HTML into a basic plain-text alternative body
        $mail->MsgHTML($msg);
        //~ $attachment = ($_REQUEST['attachment'])?$_REQUEST['attachment']:'contents.html';  
        //$attachment = 'contents.html';  
       
        if(isset($attachment) && !empty($attachment))
            $mail->AddAttachment($attachment);

        foreach(explode(",", $emailid) as $mailKey => $mailVal):
        	$mail->AddBCC($mailVal);
       		// $mail->AddAddress($mailVal);
        endforeach;

       	// $mail->AddAddress('person1@domain.com', 'Person One');
        // $ToAddress = $emailid;
        // $mail->AddAddress($ToAddress);

        
        if(!$mail->Send())
        	$mailResult = "Mailer Error: " . $mail->ErrorInfo;
        else    
        	$mailResult = "E-mail has been sent";

        return $mailResult;
    }



    /**
     * Return domain name 
     * @param  [string] $url 
     * @return [string]   Returns  Domain name 
     */
    public function url_to_domain($url){
		
	    $host = @parse_url($url, PHP_URL_HOST);
	    // If the URL can't be parsed, use the original URL
	    // Change to "return false" if you don't want that
	    if (!$host)
	        $host = $url;
	    // The "www." prefix isn't really needed if you're just using
	    // this to display the domain to the user
	    if (substr($host, 0, 4) == "www.")
	        $host = substr($host, 4);
	    // You might also want to limit the length if screen space is limited
	    // if (strlen($host) > 50)
	    //     $host = substr($host, 0, 47) . '...';
	    return $host;
	}


	//Write output to file
    public function write_file($filename, $content=array()){

    	$file = fopen($filename,"a+");
		fwrite($file,$content);
		fclose($file);
    }


    public function dd($msg){
       echo "\r\n";
       echo "[".date('Y-m-d h:i:s')."] ";
       echo $msg;
	}


	//Crawl
    public function crawl_site($url){

    	require_once 'simple_html_dom.php';


    	$context = stream_context_create();
		stream_context_set_params($context, array('user_agent' => 'Mozilla/5.0 (X11; Linux i686; rv:7.0.1) Gecko/20100101 Firefox/7.0.1'));


    	//echo $url;
    	$urlencoded = urlencode($url);
    	$html = file_get_html($url,0,$context);

    	// $this->write_file('google-crawler.txt',$html);
    	echo $html;

    	$data = array();

    	

    }

    
 	


	 /**
	 * Remove a query string parameter from an URL.
	 *
	 * @param string $url
	 * @param string $varname
	 *
	 * @return string
	 */
	public function removeQueryStringParameter($url, $varname){
	    $parsedUrl = parse_url($url);
	    $query = array();

	    if (isset($parsedUrl['query'])) {
	        parse_str($parsedUrl['query'], $query);
	        unset($query[$varname]);
	    }

	    $path = isset($parsedUrl['path']) ? $parsedUrl['path'] : '';
	    $query = !empty($query) ? '?'. http_build_query($query) : '';

	    return $parsedUrl['scheme']. '://'. $parsedUrl['host']. $path. $query;
	}


	public function generate_random_password($length = 10){
	    $alphabets = range('A','Z');
	    $numbers = range('0','9');
	    $additional_characters = array('_','.');
	    $final_array = array_merge($alphabets,$numbers,$additional_characters);
	         
	    $password = '';
	  
	    while($length--) {
	      $key = array_rand($final_array);
	      $password .= $final_array[$key];
	    }
	  
	    return $password;
	}



    public function dateDiff ($d1, $d2) {
        // Return the number of days between the two dates:
        return round(abs($d1-$d2)/86400);
    }  // end function dateDiff



    /**
     * Will identify link and hashtag from string and added achnor tag with twitter linke
     * to respective link and hashtags.
     * @param  [string] $ret [tweet to identify link and hashtag]
     * @return [string]      [tweet with linking to hashtag and external link]
     */
    public function twitterify($ret) {
        $ret = preg_replace("#(^|[\n ])([\w]+?://[\w]+[^ \"\n\r\t< ]*)#", "\\1<a href=\"\\2\" target=\"_blank\">\\2</a>", $ret);
        $ret = preg_replace("#(^|[\n ])((www|ftp)\.[^ \"\t\n\r< ]*)#", "\\1<a href=\"http://\\2\" target=\"_blank\">\\2</a>", $ret);
        //~ $ret = preg_replace("/@(\w+)/", "<a href=\"http://www.twitter.com/\\1\" target=\"_blank\">@\\1</a>", $ret);
        $ret = preg_replace("/#(\w+)/", "<a href=\"http://twitter.com/search?q=\\1\" target=\"_blank\">#\\1</a>", $ret);
        return $ret;
    }
    
    /**
     * Similar to twitter fbfy will identify link and hashtag and append fb link to string
     * @param  [string] $ret 
     * @return [string]      
     */
    public function fbfy($ret) {
        $ret = preg_replace("#(^|[\n ])([\w]+?://[\w]+[^ \"\n\r\t< ]*)#", "\\1<a href=\"\\2\" target=\"_blank\">\\2</a>", $ret);
        $ret = preg_replace("#(^|[\n ])((www|ftp)\.[^ \"\t\n\r< ]*)#", "\\1<a href=\"http://\\2\" target=\"_blank\">\\2</a>", $ret);
        //~ $ret = preg_replace("/@(\w+)/", "<a href=\"http://www.twitter.com/\\1\" target=\"_blank\">@\\1</a>", $ret);
        $ret = preg_replace("/#(\w+)/", "<a href=\"https://www.facebook.com/hashtag/\\1\" target=\"_blank\">#\\1</a>", $ret);
        return $ret;
    }


    /**
     * Load client specific data from json file
     * @param  [string] $filename [file path]
     * @return [array]  read file and convert json into array
     */
    public function load_data($file_path=null){

    	if(isset($file_path) && !empty($file_path)):
    		$load_file = fopen($file_path, "r") or die("Unable to open file!");
			$file_data = fread($load_file,filesize($file_path));
			return json_decode($file_data,true);
    	endif;


    }


    /**
     * Get list of client based on filter params.
     * @param  [array] $param [set of filtes]
     * @return [array]        [list of client]
     */
    public function get_client_name($param=array()){
        
       extract($param);

        if(isset($id) && !empty($id))
            $this->mysqli->where("id",$id);

        if(isset($client_db) && !empty($client_db))
            $this->mysqli->where("client_db",$client_db);

        if(isset($parent_id) && strlen($parent_id)>0)
            $this->mysqli->where("parent_id",$parent_id);        

        $this->mysqli->where("status",1);

        $get_clients = $this->mysqli->get(CLIENTS,null,"*");

        return $get_clients;

    }


    /**
     * Get Client specific dimention details
     * @param  array  $param [set of conditions]
     * @return [array]        [Multidimentional array of dimension values]
     */
    public function get_dimensions($param=array()){
        extract($param);

        $client_id = (isset($client_id) && !empty($client_id)) ? $client_id : CLIENT_ID;
        $status = (isset($status) && !empty($status)) ? $status : 1;
        $this->mysqli->where("client_id",$client_id);
        $this->mysqli->where("status",$status);  


        if(isset($dimension_entity) && !empty($dimension_entity)):
            if(is_array($dimension_entity))
                $this->mysqli->where("dimension_entity",$dimension_entity,"IN");
            else
                $this->mysqli->where("dimension_entity",$dimension_entity);
        endif;  
        //$this->mysqli->orderBy("display_order","ASC"); //added by nitin from confab(local)      
        $this->mysqli->orderBy("id","ASC");
        $result = $this->mysqli->get(DIMENSIONS,null,"*");
        $dimensions = array();
        if(count($result)):
            foreach($result as $key => $value):             
                if($value['parent_id']==0 && !isset($dimensions[$value['dimension_entity']])):
                    $dimensions[$value['dimension_entity']] = $value;
                else:
                    $dimensions[$value['dimension_entity']]['child'][$value['id']] = $value;
                endif;
            endforeach;
        endif;
        return $dimensions;
    }


    //Recursively get dimension value
    public function get_dimension_value($heystack,$needle,$return_attr="dimension_attribute"){

        if(isset($heystack) && is_array($heystack)):
            foreach($heystack as $key => $value):
                if($value["id"]==$needle)
                    return $value[$return_attr];
            endforeach;
        endif;
    }



    /**
     * Get client based negative keyword list.
     * @param  array  $param [set of conditions]
     * @return [array]        [list of negative keywords]
     */
    public function get_negative_keyword($param=array()){
        extract($param);

        
        $status = (isset($status) && !empty($status)) ? $status : 1;
        if(isset($client_id) && !empty($client_id))
        	$this->mysqli->where("client_id",array($client_id,0),"IN");     
        
        $this->mysqli->where("status",$status);     

        $neg_key_result = $this->mysqli->get(NEG_KEYWS,null,"id,client_id,search_term,search_type,keyw_specific,status,search_term_ref_id");
        $negative_keyw_list = array();
        if(count($neg_key_result)):
            foreach($neg_key_result as $neg_key_list_index => $neg_key_list_value):
                $negative_keyw_list[] = $neg_key_list_value;
            endforeach;
        endif;
        return $negative_keyw_list;
    }



    /**
     * get list of dimension with respective keywords
     * @param  array  $param [param key can be client_id, parent_id, keyw_id and etc]
     * @return array       
     */
    public function get_dimensions_with_keywords($param=array()){
        extract($param);
        $this->mysqli->join(KEYWS." k","k.id=kgm.keyw_id","INNER");
        $this->mysqli->join(KEYGS." kg","kg.id=kgm.keyg_id","INNER");
        $this->mysqli->join(DIMENSIONS." d","kg.keyg_name=d.dimension_attribute","INNER");


        if(isset($client_id) && !empty($client_id))
            $this->mysqli->where("kg.client_id",$client_id);

        if(isset($parent_id) && !empty($parent_id))
            $this->mysqli->where("d.parent_id",$parent_id);


        if(isset($keyw_id) && !empty($keyw_id))
            $this->mysqli->where("k.id",$keyw_id);

        if(isset($extra_not_empty) && !empty($extra_not_empty))
            $this->mysqli->where("k.extra!=''");

        $limit = (isset($limit) && !empty($limit)) ? $limit : null;

        if(isset($random) && !empty($random))
            $this->mysqli->orderBy("RAND()");

        $this->mysqli->where("d.status",1);

        $this->mysqli->where("k.status",1);

        if(isset($not_in) && !empty($not_in)):
           
           if(isset($not_in['keygroup_id']) && !empty($not_in['keygroup_id'])):
               if(is_array($not_in['keygroup_id']))
                   $this->mysqli->where("kg.id",$not_in['keygroup_id'],"NOT IN");
               else
                   $this->mysqli->where("kg.id",$not_in['keygroup_id'],"!=");
           endif;

           if(isset($not_in['keyw_id']) && !empty($not_in['keyw_id'])):
               if(is_array($not_in['keyw_id']))
                   $this->mysqli->where("k.id",$not_in['keyw_id'],"NOT IN");
               else
                   $this->mysqli->where("k.id",$not_in['keyw_id'],"!=");
           endif;           

       endif;


        $fields = "k.id as keyword_id, k.keyw_name, k.keyw_nickname, k.extra, kg.keyg_name, kg.keyg_nickname, d.dimension_attribute, d.id as dimension_id, kg.id as keygroup_id, k.keyword_variations, d.extra as dimension_extra";

        $dimensions = $this->mysqli->get(KEYG_KEYWS_MAP." kgm",$limit,$fields);
      
        $dimension_keyws = array();
        if(count($dimensions)):
            foreach($dimensions as $dKey => $dimensionInfo):
                $dimension_keyws[$dimensionInfo["dimension_id"]][] = $dimensionInfo;     
            endforeach;
        endif;

        return $dimension_keyws;
    }


    /**
     * get keyword list
     */
    public function get_keyword_list($param=array()){
        extract($param);
        $this->mysqli->join(KEYG_KEYWS_MAP." kgm","kgm.keyw_id=k.id","INNER");
        $this->mysqli->join(KEYGS." kg","kg.id=kgm.keyg_id","INNER");

        if(isset($client_id) && !empty($client_id))
        	$this->mysqli->where("kg.client_id",$client_id);

        $this->mysqli->where("k.status",1);    

        if(isset($keyw_id) && !empty($keyw_id))
            $this->mysqli->where("k.id",$keyw_id);

        if(isset($keyg_id) && !empty($keyg_id))
            $this->mysqli->where("kg.id",$keyg_id);

        if(isset($getNickname) && !empty($getNickname))
            $this->mysqli->groupBy("k.keyw_nickname");
           
        $result = $this->mysqli->get(KEYWS." k",null,"k.id,k.keyw_name,k.keyw_nickname");
        $keyword_list = array();
        if(count($result)):
            foreach($result as $key => $value):
                $keyword_list[$value["id"]] = $value;
            endforeach;
        endif;
        return $keyword_list;
    }


    public function get_content_by_url($url){
    	$domainname = $this->url_to_domain($url);
    	$plaintext = "";
    	if(isset($domainname) && !empty($domainname)):

    		$curlResponse =  $this->curlRequest($url);
    		if(isset($curlResponse) && !empty($curlResponse)):
    			$html = new simple_html_dom();  
                $html->load($curlResponse);

                if(isset($html) && !empty($html)):     
                	$selector = "";
                    switch($domainname):
                    	case 'ndtv.com':
                            $selector = "div.ins_storybody";
                            $html->find("div.highlights_wrap",0)->innertext='';
                        break;
                        case 'profit.ndtv.com':
                            $selector = "div.pdl200";
                        break;
                        case 'business-standard.com':
                            $selector = "div.story-content span.p-content";
                        break;
                        case 'smartinvestor.business-standard.com':
                            $selector = "div.mar_t_20";
                        break;  
                        case 'financialexpress.com':
                            $selector = "div.main-story-content";
                        break;

                        case 'economictimes.indiatimes.com':
                            $selector ="div.artText";
                        break;

                        case 'businesstoday.in':
                            $selector ="div#story-maincontent div.story-right";
                        break;

                        case 'bloombergquint.com':
                            $selector ="div.article__content div.article__content__card";
                        break;

                        case 'livemint.com':
                            $selector ="div#main-content div.content-box div.content";
                        break;

                        case 'indiainfoline.com':
                            $selector = "div#content_div";
                        break;


                        case 'moneycontrol.com':

                            if(strpos($url,"moneycontrol.com/video/")){
                                $selector = "div.video_wrap div.video_cnt";
                            }
                            else{
                                // $selector = "div.arti_cont div.MT20";    
                                $selector = "div.arti-flow";    
                                $html->find("div.brdr4",0)->innertext='';
                            }
                        break;
                    endswitch; 

                    if(isset($selector) && !empty($selector)):
                    	$plaintext = $html->find($selector,0)->plaintext;
                        // $this->echostr($plaintext);
                    endif;

                endif;

    		endif;
    	endif;
    	return $plaintext;
    }



}
?>