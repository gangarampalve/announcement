<?php
date_default_timezone_set('Asia/Kolkata');
include_once '/home/ubuntu/apps/ocrmypdf/code/class.confab_harvest.php';
include_once '/home/ubuntu/apps/ocrmypdf/code/MysqliDb.php';
/*include_once '/var/www/html/work/project/xp1.orm.pinstorm.com/code/class.confab_harvest.php';
include_once '/var/www/html/work/project/xp1.orm.pinstorm.com/code/MysqliDb.php';*/


use setasign\Fpdi\Fpdi;
use setasign\Fpdi\PdfReader;



require_once('/home/ubuntu/apps/ocrmypdf/vendor/setasign/fpdf/fpdf.php');
require_once('/home/ubuntu/apps/ocrmypdf/vendor/setasign/fpdi/src/autoload.php');

// DEFINE('DB_HOST_INT','35.160.66.102');
// DEFINE('DB_USER_INT','premium');
// DEFINE('DB_PASSWORD_INT','pre2m0i1u0m');
// DEFINE('DB_CONFAB_APP','confab_intext_nseit');

DEFINE('DB_HOST_INT','confabdblive.cwamjpgnol0x.us-west-2.rds.amazonaws.com');
DEFINE('DB_USER_INT','confabium');
DEFINE('DB_PASSWORD_INT','hQ7+cC&%ekLU');
DEFINE('DB_CONFAB_APP','confab_intext_nseit');


$mysqli_int = new Mysqlidb (DB_HOST_INT, DB_USER_INT, DB_PASSWORD_INT, DB_CONFAB_APP);

$harvest = new Harvest($mysqli_int);

//34123104
$file_path 		= '/home/ubuntu/apps/ocrmypdf/storage/';
$gs_storage 	= '/home/ubuntu/apps/ocrmypdf/ghostScriptPDF/';
$splits_storage = '/home/ubuntu/apps/ocrmypdf/splits/';

$startdate = strtotime(date('Y-m-d')." -1 day");
$startdate = strtotime("midnight");
$enddate   = strtotime("tomorrow", $startdate) - 1;


// $enddate              = strtotime('17:00:00');
// $startdate          = strtotime('-1 days 17.00.01', $enddate);

// $enddate              = strtotime('17:29:00');
// $startdate          = strtotime('-1 day 17.30.00', $enddate);
// $mysqli_int->where("m.url LIKE https://nseindia.com/%");
$mysqli_int->where("url LIKE '%nseindia.com/%'");
// $mysqli_int->where('platform',"nseindia.com");
$mysqli_int->where('status',array(0,7),"IN");
$mysqli_int->where("post_time",array($startdate,$enddate),"BETWEEN");
// $mysqli_int->where("post_time",array('1552991446','1553495455'),"BETWEEN");
$mysqli_int->orderBy("RAND ()");
$mysqli_int->where("og_image",'',"!=");
$mysqli_int->where("og_image",'404',"!=");
// $mysqli_int->where("id","34219122");
$result = $mysqli_int->getOne("mentions","id,title,url,og_image");
$harvest->printdata($result);
if(count($result)):
	$harvest->echostr("Result Found");
		if(file_exists($file_path.$result['og_image'])):
			$image = new Imagick();
			$image->pingImage($file_path.$result['og_image']);
			$nos_pdf_page = $image->getNumberImages();
			if($nos_pdf_page<=5):
				$harvest->echostr($result['og_image']." Page Count ".$nos_pdf_page);
				$pdf_name = explode('.', $result['og_image']);
				$txt_filename = $file_path.$pdf_name[0].'.txt';
				//executing python script to get text out of pdf using ocrmypdf.
				shell_exec('ocrmypdf --tesseract-timeout 300 --force-ocr  --sidecar '.$txt_filename .' '.$file_path.$result['og_image'].' '.$file_path.$pdf_name[0].'_ocr.pdf');

				if (file_exists($txt_filename)):
					$fp = fopen($txt_filename, 'r');
					$announcement = fread($fp, filesize($txt_filename));
					
					$str1 = preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $announcement);
					$desc = preg_replace("/[^a-zA-Z0-9]+/", " ", $str1);
					if(!$mysqli_int->ping())
						$mysqli_int = $mysqli_int->mysqli();

					$update_data = array(
										"og_image" 		=> '',
										"description" 	=> $desc
										);
					$mysqli_int->where("id",$result['id']);
					$mysqli_int->update("mentions",$update_data,1);
					$harvest->echostr("Updated mention :".$result['id']);
					unlink($txt_filename);
					unlink($file_path.$pdf_name[0].'_ocr.pdf');
					unlink($file_path.$result['og_image']);
				endif;
			else:

				$harvest->echostr($result['og_image']." Page Count ".$nos_pdf_page);
				if(file_exists($file_path.$result['og_image'])):
					$filename = $gs_storage."gs_".$result['og_image'];
					shell_exec( "gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dNOPAUSE -dQUIET -dBATCH -sOutputFile=".$filename." ".$file_path.$result['og_image']);
					if(file_exists($filename)):


						$pdf = new Fpdi();
						// print_r($pdf);exit;
						$pagecount = $pdf->setSourceFile($filename); // How many pages?
						
						// Split each page into a new PDF
						$description = '';
						for ($i = 1; $i <= 5; $i++) {
							$desc = '';
							$new_pdf = new Fpdi();
							$new_pdf->AddPage();
							$new_pdf->setSourceFile($filename);
							$new_pdf->useTemplate($new_pdf->importPage($i));
							
							try {
								$new_filename = $splits_storage.$i.".pdf";
								$new_pdf->Output($new_filename, "F");
								echo "Page ".$i." split into ".$new_filename."<br />\n";
								shell_exec('ocrmypdf --tesseract-timeout 300 --force-ocr  --sidecar '.$file_path.$i.'.txt '.$new_filename.' '.$file_path.$i.'_ocr.pdf');
								if(file_exists($file_path.$i.'.txt')):
									$fp = fopen($file_path.$i.'.txt', 'r');
									$announcement = fread($fp, filesize($file_path.$i.'.txt'));
									
									$str1 = preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $announcement);
									$desc = preg_replace("/[^a-zA-Z0-9]+/", " ", $str1);
									
									unlink($file_path.$i.'.txt');
									unlink($file_path.$i.'_ocr.pdf');
									unlink($new_filename);
									
								endif;
							} catch (Exception $e) {
								echo 'Caught exception: ',  $e->getMessage(), "\n";
							}
							$description.=$desc;
						}
						if(!$mysqli_int->ping())
							$mysqli_int = $mysqli_int->mysqli();

						if(isset($description) && !empty($description)){
							$update_data = array(
												"og_image" 		=> '',
												"description" 	=> $description
											);

							$harvest->printdata($update_data);
							unlink($filename);
							unlink($file_path.$result['og_image']);
							
							$mysqli_int->where("id",$result['id']);
							$mysqli_int->update("mentions",$update_data,1);
							$harvest->echostr("Updated mention :".$result['id']);
						}
						
					endif;
					
				endif;				
			endif;
		endif;
		
endif;

?>