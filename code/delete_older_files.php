<?php
date_default_timezone_set('Asia/Kolkata');
  $path = '/home/ubuntu/apps/ocrmypdf/storage/';
  if ($handle = opendir($path)) {
     while (false !== ($file = readdir($handle))) {
        if ((time()-filectime($path.$file)) >= 172800) {  
           if (preg_match('/^.*\.(pdf|zip|csv|txt)$/i', $file)) {
              unlink($path.$file);
              echo $file."\n\r";
           }
        }
     }
     
   }
?>